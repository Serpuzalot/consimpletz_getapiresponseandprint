﻿using Microsoft.Extensions.Configuration;
using System;
using System.Text.Json;

namespace ConsimpleTZ
{
    class Program
    {
        public static IConfiguration _configuration;

        public static async Task Main(string[] args)
        {
            Configuration();
            MakeTable makeTable = new MakeTable();
            string result = await makeTable.GetCreatedTable();
            Console.WriteLine(result);
        }

        public static void Configuration()
        {
            string filePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            filePath =  Directory.GetCurrentDirectory();
            
            for (int i = 0; i < 3; i++)
            {
                DirectoryInfo dirInfo = Directory.GetParent(filePath);
                filePath = dirInfo.FullName;
            }
            
            _configuration = new ConfigurationBuilder()
            .AddJsonFile($"{filePath}\\appsettings.json", false,true)
            .Build();

            if (String.IsNullOrEmpty(_configuration["apiURL"]))
            {
                string apiURL = "http://tester.consimple.pro";
                AppSettingsJson json = new AppSettingsJson();
                json.apiURL = apiURL;
                string jsonString = JsonSerializer.Serialize(json);
                System.IO.File.WriteAllText($"{filePath}\\appsettings.json", jsonString);
                _configuration = new ConfigurationBuilder()
                    .AddJsonFile($"{filePath}\\appsettings.json", false, true)
                    .Build();


            }


        }

    }
}