﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsimpleTZ
{
    public class JsonData
    {
        public IList<Products> Products { get; set; }
        public IList<Categories> Categories { get; set; }  

    }

    public class Products
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
    }

    public class Categories
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class AppSettingsJson
    {
        public string apiURL { get; set; }
    }
}
