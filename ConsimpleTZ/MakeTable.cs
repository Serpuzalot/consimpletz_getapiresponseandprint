﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace ConsimpleTZ
{
    public  class MakeTable
    {
        static readonly HttpClient _client = new HttpClient();

        private async Task<JsonData> GetDataFromApi()
        {
            HttpResponseMessage response = await _client.GetAsync(Program._configuration["apiURL"]);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            JsonData data = JsonSerializer.Deserialize<JsonData>(responseBody);
            return data;
        }

        public async Task<string> GetCreatedTable()
        {
            string table = String.Format("|{0,20}|{1,20}|", "Product name", "Category name")+"\n";
            for (int i = 0; i < 22; i++)
            {
                table += "_ ";
            }
            table += "\n";
            JsonData data = await GetDataFromApi();
            if(data.Products == null && data.Categories == null)
            {
                return "No Data!";
            }
            foreach(var el in data.Products)
            {
                string productName = el.Name;
                string categoryName = data.Categories.FirstOrDefault(x => x.Id == el.CategoryId).Name;
                if (productName.Length > 20)
                {
                    productName = el.Name.Substring(0,17);
                    productName += "...";
                }
                if(categoryName.Length > 20)
                {
                    categoryName = categoryName.Substring(0, 17);
                    categoryName += "...";
                }
                table += String.Format("|{0,20}|{1,20}|", productName,categoryName) + "\n";
                for(int i = 0; i < 22; i++)
                {
                    table += "_ ";
                }
                table += "\n";
            }
            return table;

        }

    }
}
